import java.util.ArrayList;
import java.util.Scanner;

public class MainTask {
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		ArrayList<Task> arr = new ArrayList<>();
		Scanner ip = new Scanner(System.in);
		
		TaskController a = new TaskController();
		int choice=0;
		String s="yes";
		do {
			System.out.println("1.Add Task");
			System.out.println("2.Search Title ");
			System.out.println("3.Search Text ");
			System.out.println("4.Search AssignedTo ");
			System.out.println("5.Update Title ");
			System.out.println("6.Update Text ");
			System.out.println("7.Update AssignedTo ");
			System.out.println("8.Delete Title ");
			System.out.println("9.Delete Text ");
			System.out.println("10.Delete AssignedTo ");
			System.out.println("11.Print Task ");
			System.out.println("you can choose function from 1 to 11 : ");
			choice = ip.nextInt();
			
			switch (choice) {
			case 1:{
				a.Add(arr);
				break;
			}
			case 2:{
				a.SearchDisplayTitle(arr);
				break;
			}
			case 3:{
				a.SearchDisplayText(arr);;
				break;
			}
			case 4:{
				a.SearchDisplayAssignedTo(arr);;
				break;
			}
			case 5:{
				a.UpdateTitle(arr);
				break;
			}
			case 6:{
				a.UpdateText(arr);
				break;
			}
			case 7:{
				a.UpdateAssignedTo(arr);
				break;
			}
			case 8:{
				a.DeleteTitle(arr);
				break;
			}
			case 9:{
				a.DeleteText(arr);
				break;
			}
			case 10:{
				a.DeleteAssignedTo(arr);
				break;
			}
			case 11:{
				a.Print(arr);
				break;
			}
			case 0 : {
				System.exit(1);
			}

			default:{
				System.out.println("Choice 1 to 11");
				break;
			}
			}
			
				
			System.out.println("Would you like to continue?(yes/no)");
			s=ip.next();
			
			
		}while(s.equals("yes"));
		

	}

}
