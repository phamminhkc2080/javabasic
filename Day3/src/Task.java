
public class Task {
	private int Id;
	private String Title;
	private String Text;
	private String assignedTo;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getText() {
		return Text;
	}
	public void setText(String text) {
		Text = text;
	}
	public String getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "\n ID : "+Id+"\n"+"Title : "+Title+"\n"+"Text : "+Text+"\n"+"assignedTo : "+assignedTo;
	}
	public Task(int id, String title, String text, String assignedTo) {
		Id = id;
		Title = title;
		Text = text;
		this.assignedTo = assignedTo;
	}
	
	
	
}
