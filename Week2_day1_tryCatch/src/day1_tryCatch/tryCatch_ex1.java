package day1_tryCatch;

import java.util.Random;
import java.util.Scanner;

public class tryCatch_ex1 {
	
	
	public static void main(String[] args) {
		Scanner ip = new Scanner(System.in);
		int n ;
		int arr [] ;
		int Nbdiv;
		Random rd = new Random();
		System.out.print("Enter the number of arrays : ");
		 n = ip.nextInt();
		arr = new int[n];
		
			
		try {
			System.out.println("Enter to add number : ");
			for(int i=0;i<arr.length;i++) {
				System.out.println("Task"+(i+1)+" : ");
				arr[i] = ip.nextInt();	
			}
			System.out.println("Print Random : " + arr[rd.nextInt(arr.length)]);
			System.out.println("Enter divisor: ");
			Nbdiv = ip.nextInt();
			try {
				for(int i=0;i<arr.length;i++) {
					System.out.println("result " + arr[i]/Nbdiv) ;
				}
			}catch (ArithmeticException e) {
				
				// TODO: handle exception
				System.out.println("Error : "+ e);
			}
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Error : "+e);
			// TODO: handle exception
		}
			
		
	}

}
