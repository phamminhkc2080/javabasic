package day1_tryCatch;

import java.util.Scanner;

public class tryCatch_ex2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner ip = new Scanner(System.in);
		int arr[];
		int n;
		int div;
		try {
			System.out.print("Enter the number of arrays : ");
			 n = ip.nextInt();
			arr = new int[n];
			try {
				System.out.println("Enter to add number : ");
				for(int i=0;i<arr.length;i++) {
					System.out.println("Task"+(i+1)+" : ");
					arr[i] = ip.nextInt();	
				}
				System.out.println("Enter divisor: ");
				div = ip.nextInt();
				try {
					for(int i=0;i<arr.length;i++) {
						System.out.println("result " + arr[i]/div) ;
					}
				}catch (ArithmeticException e) {
					
					// TODO: handle exception
					System.out.println("Error : "+ e);
				}
			}catch (ArrayIndexOutOfBoundsException e) {
				System.err.println("Error : "+ e );
				// TODO: handle exception
			}
		}catch (Exception e) {
			// TODO: handle exception
			System.err.println("Error "+e);
		}
			
	}

}
